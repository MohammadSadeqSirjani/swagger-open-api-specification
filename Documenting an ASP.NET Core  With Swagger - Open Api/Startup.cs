using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Library.Context;
using Library.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;

namespace Library
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddMvc(setup =>
            {
                setup.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status500InternalServerError));
                setup.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status400BadRequest));
                setup.Filters.Add(new ProducesResponseTypeAttribute(StatusCodes.Status406NotAcceptable));
                setup.Filters.Add(new ProducesDefaultResponseTypeAttribute());

                setup.ReturnHttpNotAcceptable = true;

                setup.OutputFormatters.Add(new XmlSerializerOutputFormatter());

                var jsonOutputFormatter = setup.OutputFormatters
                    .OfType<SystemTextJsonOutputFormatter>().FirstOrDefault();

                // remove text/json as it isn't the approved media type
                // for working with JSON at API level
                if (jsonOutputFormatter == null) return;
                if (jsonOutputFormatter.SupportedMediaTypes.Contains("text/json"))
                {
                    jsonOutputFormatter.SupportedMediaTypes.Remove("text/json");
                }
            }).SetCompatibilityVersion(CompatibilityVersion.Latest);

            var connectionString = Configuration.GetSection("ConnectionStrings:LibraryDBConnectionString").Value;
            services.AddDbContext<LibraryContext>(option => option.UseSqlServer(connectionString));

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    // if there are model state errors & all keys were correctly
                    // found/parsed we're dealing with validation errors
                    if (context is ActionExecutingContext actionExecutingContext &&
                        actionExecutingContext.ModelState.ErrorCount > 0 &&
                        actionExecutingContext.ModelState.ErrorCount == context.ModelState.ErrorCount)
                    {
                        return new UnprocessableEntityObjectResult(context.ModelState);
                    }

                    // if one of the keys wasn't correctly found / couldn't be parsed
                    // we're dealing with null/unparseable input
                    return new BadRequestObjectResult(context.ModelState);
                };
            });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("LibraryOpenAPISpecification",
                    new OpenApiInfo
                    {
                        Title = "Library API",
                        Version = "1",
                        Description = "Through this API you can access authors and their books.",
                        Contact = new OpenApiContact()
                        {
                            Url = new Uri("https://t.me/Mohammad_Sadeq_Sirjani"),
                            Name = "Mohammad Sadeq Sirjani",
                            Email = "m.sadeq.sirjani@gmail.com"
                        },
                        License = new OpenApiLicense()
                        {
                            Url = new Uri("https://opensoure.org/licenses/MIT"),
                            Name = "MIT License"
                        }
                    });

                var xmlCommentFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlCommentFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentFile);
                options.IncludeXmlComments(xmlCommentFullPath);
            });

            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<IBookRepository, BookRepository>();

            services.AddAutoMapper(Assembly.GetAssembly(GetType()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/LibraryOpenAPISpecification/swagger.json", "Library API");
                options.RoutePrefix = "";
            });

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
