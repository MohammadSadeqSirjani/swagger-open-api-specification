﻿using AutoMapper;

namespace Library.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Entities.Book, Models.BookWithConcatenatedAuthorName>()
                .ForMember(destination => destination.Author,
                    options =>
                        options.MapFrom(source => $"{source.Author.Forename}{source.Author.Surname}"));

            CreateMap<Entities.Book, Models.Book>()
                .ForMember(destination => destination.AuthorForename,
                    option =>
                        option.MapFrom(source => $"{source.Author.Forename}")).ForMember(
                    destination => destination.AuthorSurname,
                    option =>
                        option.MapFrom(source => $"{source.Author.Surname}"));

            CreateMap<Models.BookForCreation, Entities.Book>();

            CreateMap<Models.BookForCreationWithAmountOfPages, Entities.Book>();
        }
    }
}
