﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library.Context;
using Library.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library.Services
{
    public class AuthorRepository : IAuthorRepository, IDisposable
    {
        private LibraryContext _context;

        public AuthorRepository(LibraryContext context)
        {
            _context = context;
        }
        public async Task<bool> AuthorExistsAsync(Guid authorId) =>
            await _context.Authors.AsNoTracking().AnyAsync(a => a.Id == authorId);

        public async Task<IEnumerable<Author>> GetAuthorsAsync() => await _context.Authors.ToListAsync();

        public async Task<Author> GetAuthorAsync(Guid authorId)
        {
            if (authorId == Guid.Empty) throw new ArgumentException(nameof(authorId));

            return await _context.Authors.AsNoTracking().FirstOrDefaultAsync(a => a.Id == authorId);
        }

        public void UpdateAuthor(Author author)
        {
            _context.Authors.Update(author);
        }

        public void UpdateAuthorAsync(Author author)
        {
        }

        public async Task<bool> SaveChangesAsync() => await _context.SaveChangesAsync() > 0;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing || _context == null) return;
            _context.Dispose();
            _context = null;
        }
    }
}
