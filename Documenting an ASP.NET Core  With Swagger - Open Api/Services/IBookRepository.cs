﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library.Entities;

namespace Library.Services
{
    public interface IBookRepository : IDisposable
    {
        Task<IEnumerable<Book>> GetBooksAsync(Guid authorId);

        Task<Book> GetBookAsync(Guid authorId, Guid bookId);

        Task AddBook(Book book);

        Task<bool> SaveChangesAsync();
    }
}
