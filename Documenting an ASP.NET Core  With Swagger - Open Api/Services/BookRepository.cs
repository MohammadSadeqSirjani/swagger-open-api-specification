﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Context;
using Library.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library.Services
{
    public class BookRepository : IBookRepository, IDisposable
    {
        private LibraryContext _context;

        public BookRepository(LibraryContext context)
        {
            _context = context ?? throw new ArgumentException(nameof(context));
        }

        public async Task<IEnumerable<Book>> GetBooksAsync(Guid authorId)
        {
            if (authorId == Guid.Empty) throw new ArgumentException(nameof(authorId));

            return await _context.Books
                .Include(b => b.Author)
                .Where(b => b.Author.Id == authorId)
                .ToListAsync();
        }

        public async Task<Book> GetBookAsync(Guid authorId, Guid bookId)
        {
            if (bookId == Guid.Empty) throw new ArgumentException(nameof(bookId));

            if (authorId == Guid.Empty) throw new ArgumentException(nameof(authorId));

            return await _context.Books
                .Include(b => b.Author)
                .Where(b => b.Author.Id == authorId && b.Id == bookId)
                .FirstOrDefaultAsync();
        }

        public async Task AddBook(Book book)
        {
            if (book == null) throw new ArgumentException(nameof(book));

            await _context.Books.AddAsync(book);
        }

        public async Task<bool> SaveChangesAsync() => await _context.SaveChangesAsync() > 0;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing || _context == null) return;
            _context.Dispose();
            _context = null;
        }
    }
}
