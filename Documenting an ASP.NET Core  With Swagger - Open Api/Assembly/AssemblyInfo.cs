﻿using Microsoft.AspNetCore.Mvc;

// Add Api convention type globally into assembly file for all controller
// It is used for simple, basic API for complex one use attributes
//[assembly: ApiConventionType(typeof(DefaultApiConventions))]
