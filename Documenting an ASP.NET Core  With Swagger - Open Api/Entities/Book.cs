﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Library.Entities
{
    public class Book
    {
        [Key] public Guid Id { get; set; }

        [Required, StringLength(150)] public string Title { get; set; }

        [Required, StringLength(2500)] public string Description { get; set; }

        public int? AmountOfPages { get; set; }

        public Guid AuthorId { get; set; }

        public Author Author { get; set; }
    }
}
