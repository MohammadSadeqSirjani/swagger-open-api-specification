﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.Entities
{
    public class Author
    {
        [Key] public Guid Id { get; set; }

        [Required, StringLength(150)] public string Forename { get; set; }

        [Required, StringLength(150)]
        public string Surname { get; set; }

        public ICollection<Book> Books { get; set; } = new List<Book>();
    }
}
