﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.Models;
using Library.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Library.Controllers
{
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public class BookController : AdvancedApiController
    {
        private readonly IBookRepository _bookRepository;
        private readonly IAuthorRepository _authorRepository;

        public BookController(IBookRepository bookRepository, IAuthorRepository authorRepository, IMapper mapper) : base(mapper)
        {
            _bookRepository = bookRepository;
            _authorRepository = authorRepository;
        }

        /// <summary>
        /// Get all books details by author
        /// </summary>
        /// <param name="authorId">The author id</param>
        /// <returns></returns>
        [HttpGet("[action]/{authorId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Book>>> GetBooks([FromRoute] Guid authorId)
        {
            if (!await _authorRepository.AuthorExistsAsync(authorId))
                return NotFound();

            var bookFromRepo = await _bookRepository.GetBooksAsync(authorId);
            return Ok(Mapper.Map<IEnumerable<Book>>(bookFromRepo));
        }

        /// <summary>
        /// Get specific book information
        /// </summary>
        /// <param name="authorId">The author id</param>
        /// <param name="bookId">The book id</param>
        /// <returns></returns>
        [HttpGet("[action]/{authorId}/{bookId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Book>> GetBook([FromRoute] Guid authorId, [FromRoute] Guid bookId)
        {
            if (!await _authorRepository.AuthorExistsAsync(authorId)) return NotFound();

            var bookFromRepo = await _bookRepository.GetBookAsync(authorId, bookId);
            if (bookFromRepo == null) return NotFound();

            return Ok(Mapper.Map<Book>(bookFromRepo));
        }

        /// <summary>
        /// Get a book by id for a specific author
        /// </summary>
        /// <param name="authorId">The id of the book author</param>
        /// <param name="bookId">The id of the book</param>
        /// <returns>An ActionResult of type BookWithConcatenatedAuthorName</returns>
        [HttpGet("[action]/{authorId}/{bookId}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetBookWithConcatenatedAuthorName(
            [FromRoute] Guid authorId, [FromRoute] Guid bookId)
        {
            if (!await _authorRepository.AuthorExistsAsync(authorId)) return NotFound();

            var bookFromRepo = await _bookRepository.GetBookAsync(authorId, bookId);
            if (bookFromRepo == null) return NotFound();

            return Ok(Mapper.Map<BookWithConcatenatedAuthorName>(bookFromRepo));
        }

        /// <summary>
        /// Create a book
        /// </summary>
        /// <param name="bookForCreation">More detail for creating new book</param>
        /// <returns></returns>
        /// <response code = "422">Validation error</response>
        [HttpPost("[action]")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity, 
            Type = typeof(Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary))]
        public async Task<ActionResult<Book>> CreateBook([FromBody] BookForCreation bookForCreation)
        {
            if (!await _authorRepository.AuthorExistsAsync(bookForCreation.AuthorId))
                return NotFound();

            var bookToAdd = Mapper.Map<Entities.Book>(bookForCreation);
            await _bookRepository.AddBook(bookToAdd);
            await _bookRepository.SaveChangesAsync();

            var bookFromRepo = await _bookRepository.GetBookAsync(bookToAdd.AuthorId, bookToAdd.Id);
            if (bookFromRepo == null) NotFound();

            return Ok(Mapper.Map<Book>(bookFromRepo));
        }

        /// <summary>
        /// Create a book for a specific author
        /// </summary>
        /// <param name="bookForCreationWithAmountOfPages">The book to create</param>
        /// <returns>An ActionResult of type Book</returns>
        /// <response code="422">Validation error</response>
        [HttpPost("[action]")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity,
            Type = typeof(Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary))]
        public async Task<ActionResult<Book>> CreateBookWithAmountOfPages(
            [FromBody] BookForCreationWithAmountOfPages bookForCreationWithAmountOfPages)
        {
            if (!await _authorRepository.AuthorExistsAsync(bookForCreationWithAmountOfPages.AuthorId))
            {
                return NotFound();
            }

            var bookToAdd = Mapper.Map<Entities.Book>(bookForCreationWithAmountOfPages);
            await _bookRepository.AddBook(bookToAdd);
            await _bookRepository.SaveChangesAsync();

            var bookFromRepo = await _bookRepository.GetBookAsync(bookToAdd.AuthorId, bookToAdd.Id);
            if (bookFromRepo == null) NotFound();

            return Ok(Mapper.Map<Book>(bookFromRepo));
        }
    }
}