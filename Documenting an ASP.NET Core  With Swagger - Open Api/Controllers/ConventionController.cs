﻿//using System.Collections.Generic;
//using AutoMapper;
//using Microsoft.AspNetCore.Mvc;
//using Library.Convention;

//namespace Library.Controllers
//{
//    public class ConventionController : AdvancedApiController
//    {
//        public ConventionController(IMapper mapper) : base(mapper)
//        {
//        }

//        // GET: api/Convention
//        [HttpGet]
//        //[ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
//        public IEnumerable<string> Get()
//        {
//            return new string[] { "value1", "value2" };
//        }

//        // GET: api/Convention/5
//        [HttpGet("{id}", Name = "Get")]
//        //[ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
//        public string Get(int id)
//        {
//            return "value";
//        }

//        // POST: api/Convention
//        [HttpPost]
//        [ApiConventionMethod(typeof(CustomConvention), nameof(CustomConvention.Insert))]
//        public void Insert([FromBody] string value)
//        {
//        }

//        // PUT: api/Convention/5
//        [HttpPut("{id}")]
//        //[ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Update))]
//        public void Put(int id, [FromBody] string value)
//        {
//        }

//        // DELETE: api/ApiWithActions/5
//        [HttpDelete("{id}")]
//        //[ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Delete))]
//        public void Delete(int id)
//        {
//        }
//    }
//}
