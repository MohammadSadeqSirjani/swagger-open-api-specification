﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Library.Controllers
{
    [Route("api/[controller]")] 
    [ApiController]
    [Produces("application/json", "application/xml")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public class AdvancedApiController : ControllerBase
    {
        protected readonly IMapper Mapper;

        public AdvancedApiController(IMapper mapper)
        {
            Mapper = mapper;
        }
    }
}