﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.Models;
using Library.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Library.Controllers
{
    public class AuthorController : AdvancedApiController
    {
        private readonly IAuthorRepository _authorRepository;

        public AuthorController(IAuthorRepository authorRepository, IMapper mapper) : base(mapper)
        {
            _authorRepository = authorRepository;
        }

        /// <summary>
        /// Get all authors name
        /// </summary>
        /// <returns>An ActionResult of type Authors</returns>
        /// <response code = "200">Return the requested authors</response>
        [HttpGet("[action]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Author>>> GetAuthors()
        {
            var authorsFromRepo = await _authorRepository.GetAuthorsAsync();

            return Ok(Mapper.Map<IEnumerable<Author>>(authorsFromRepo));
        }

        /// <summary>
        /// Get an author by his/her id
        /// </summary>
        /// <param name="authorId">The id of the author you want to get</param>
        /// <returns>An ActionResult of type Author</returns>
        /// <response code = "200">Return the requested author</response>
        [HttpGet("[action]/{authorId}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Author>> GetAuthor([FromRoute] Guid authorId)
        {
            var authorFromRepo = await _authorRepository.GetAuthorAsync(authorId);

            if (authorFromRepo == null) return NotFound();

            return Ok(Mapper.Map<Author>(authorFromRepo));
        }

        /// <summary>
        /// Update author information
        /// </summary>
        /// <param name="authorId">The id of the author you want to update</param>
        /// <param name="authorForUpdate">item you can update for author</param>
        /// <returns></returns>
        /// <response code = "200">Update the requested author</response>
        [HttpPut("[action]")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity, 
            Type = typeof(Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary))]
        public async Task<ActionResult<Author>> UpdateAuthor([FromRoute] Guid authorId, [FromBody] AuthorForUpdate authorForUpdate)
        {
            var authorFromRepo = await _authorRepository.GetAuthorAsync(authorId);
            if (authorFromRepo == null)
                return NotFound();

            Mapper.Map(authorForUpdate, authorFromRepo);

            _authorRepository.UpdateAuthor(authorFromRepo);
            await _authorRepository.SaveChangesAsync();

            return Ok(Mapper.Map<Author>(authorFromRepo));
        }

        /// <summary>
        /// Partially update an author
        /// </summary>
        /// <param name="authorId">The id of the author you want to get</param>
        /// <param name="patchDocument">The set of operations to apply to the author</param>
        /// <returns>An ActionResult of type Author</returns>
        /// <remarks>
        /// Sample request (this request updates the author's first name) 
        /// PATCH /authors/id 
        /// [ 
        ///     { 
        ///       "op": "replace", 
        ///       "path": "/firstname", 
        ///       "value": "new first name" 
        ///       } 
        /// ]  
        /// </remarks>
        /// <response code = "200">Update the requested author</response>
        [HttpPatch("[action]/{authorId}")]
        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity, 
            Type = typeof(Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary))]
        public async Task<ActionResult<Author>> UpdateAuthor(Guid authorId, JsonPatchDocument<AuthorForUpdate> patchDocument)
        {
            var authorFromRepo = await _authorRepository.GetAuthorAsync(authorId);
            if (authorFromRepo == null)
                return NotFound();

            // map to DTO to apply the patch to
            var author = Mapper.Map<AuthorForUpdate>(authorFromRepo);
            patchDocument.ApplyTo(author);

            // if there are errors when applying the patch the patch doc 
            // was badly formed  These aren't caught via the ApiController
            // validation, so we must manually check the model state and
            // potentially return these errors.
            if (!ModelState.IsValid)
                return new UnprocessableEntityObjectResult(ModelState);

            // map the applied changes on the DTO back into the entity
            Mapper.Map(author, authorFromRepo);

            // update & save
            _authorRepository.UpdateAuthor(authorFromRepo);
            await _authorRepository.SaveChangesAsync();

            // return the author
            return Ok(Mapper.Map<Author>(authorFromRepo));
        }
    }
}