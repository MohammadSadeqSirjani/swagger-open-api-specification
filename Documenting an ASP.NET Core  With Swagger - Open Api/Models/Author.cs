﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    /// <summary>
    /// An author with id, firstName, lastName fields
    /// </summary>
    public class Author
    {
        /// <summary>
        /// The id of author
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The first name of author
        /// </summary>
        [Required, MaxLength(150)]
        public string Forename { get; set; }

        /// <summary>
        /// The last name of author
        /// </summary>
        [Required, MaxLength(150)]
        public string Surname { get; set; }
    }
}
