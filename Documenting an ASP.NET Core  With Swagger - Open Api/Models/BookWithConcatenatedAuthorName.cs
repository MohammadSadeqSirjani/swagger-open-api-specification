﻿using System;

namespace Library.Models
{
    /// <summary>
    /// The model for creation of book with concatenating author name 
    /// </summary>
    public class BookWithConcatenatedAuthorName
    {
        /// <summary>
        /// The book id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The complete author name
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// The book name
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The book description
        /// </summary>
        public string Description { get; set; }
    }
}
