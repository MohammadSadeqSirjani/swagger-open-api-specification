﻿using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    /// <summary>
    /// The model for updating author details
    /// </summary>
    public class AuthorForUpdate
    {
        /// <summary>
        /// The first name of author
        /// </summary>
        [Required, MaxLength(150)]
        public string Forename { get; set; }

        /// <summary>
        /// The last name of authorS
        /// </summary>
        [Required, MaxLength(150)]
        public string Surname { get; set; }
    }
}
