﻿namespace Library.Models
{
    /// <summary>
    /// The book model for creating book with amount of pages
    /// </summary>
    public class BookForCreationWithAmountOfPages : BookForCreation
    {
        /// <summary>
        /// The count of book pages
        /// </summary>
        public int AmountOfPages { get; set; }
    }
}
