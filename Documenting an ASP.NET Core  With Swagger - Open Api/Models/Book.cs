﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    /// <summary>
    /// The book model for binding book and author name
    /// </summary>
    public class Book
    {
        /// <summary>
        /// The book id
        /// </summary>
        [Required]
        public Guid Id { get; set; }

        /// <summary>
        /// The first name of author
        /// </summary>
        [Required, MaxLength(150)]
        public string AuthorForename { get; set; }

        /// <summary>
        /// The last name of author
        /// </summary>
        [Required, MaxLength(150)]
        public string AuthorSurname { get; set; }

        /// <summary>
        /// The title of book
        /// </summary>
        [Required, MaxLength(150)]
        public string Title { get; set; }

        /// <summary>
        /// The description about book 
        /// </summary>
       [Required, MaxLength(2500)]
        public string Description { get; set; }
    }
}
