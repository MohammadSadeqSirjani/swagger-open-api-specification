﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    /// <summary>
    /// The book creation model for binding more detail about book
    /// </summary>
    public class BookForCreation
    {
        /// <summary>
        /// The author id
        /// </summary>
        [Required]
        public Guid AuthorId { get; set; }

        /// <summary>
        /// The book name
        /// </summary>
        [Required, MaxLength(150)]
        public string Title { get; set; }

        /// <summary>
        /// The book details
        /// </summary>
        [Required, MaxLength(2500)]
        public string Description { get; set; }
    }
}
